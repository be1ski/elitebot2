import data.PropertiesRepo
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi
import presentation.EliteBot

fun main(args: Array<String>) {
  val logger = LoggerFactory.getLogger("Application")

  logger.info("App started")

  ApiContextInitializer.init()
  logger.info("AppContext initialized")

  val api = TelegramBotsApi()
  val elitebot = EliteBot(PropertiesRepo().properties)

  api.registerBot(elitebot)

  logger.info("Elitebot ready!")
}