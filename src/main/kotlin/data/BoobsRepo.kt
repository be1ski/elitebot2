package data

import data.CachedImageLoader.LoadResult
import data.CachedImageLoader.LoadResult.LoadFailure
import data.CachedImageLoader.LoadResult.LoadSuccess
import java.io.File
import java.util.Random
import org.json.JSONArray
import org.apache.http.impl.client.BasicResponseHandler
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit


class BoobsRepo(properties: PropertiesRepo.Properties) {
  private val logger = LoggerFactory.getLogger(javaClass)
  private val httpClient = HttpClientBuilder.create().build()
  private val imageLoader = CachedImageLoader(properties)
  private val random = Random()

  private var lastSync = 0L
  private val maxBoobsId = fetchMaxBoobsId()

  init {
    logger.info("BoobsRepo initialized")
  }

  fun nextBoobs() : BoobsResult {
    if((System.currentTimeMillis() - lastSync) > TimeUnit.DAYS.toMillis(1)) {
      fetchMaxBoobsId()
    }
    logger.info("Throw a coin...")
    val nextBoobsId = random.nextInt(maxBoobsId)
    logger.info("Next boobsId: $nextBoobsId")
    val loadResult =
        imageLoader.load(String.format("http://media.oboobs.ru/boobs/%05d.jpg", nextBoobsId), "boobs")
    return processLoadResult(loadResult)
  }

  private fun processLoadResult(loadResult: LoadResult) = when(loadResult) {
    is LoadSuccess -> BoobsResult.Success(loadResult.file)
    is LoadFailure -> BoobsResult.Failure("Попробуй снова")
  }

  private fun fetchMaxBoobsId(): Int {
    logger.info("Start fetching maxBoobsId")
    try { val responseBody = httpClient.execute(HttpGet("http://api.oboobs.ru/boobs/0/1/-id"), BasicResponseHandler())
      val result = JSONArray(responseBody).getJSONObject(0).getInt("id")
      logger.info("Fetch maxBoobsId success: $result")
      lastSync = System.currentTimeMillis()
      return result
    } catch (e: Exception) { logger.info("maxBoobsId fetching failed $e") }
    return 0
  }

  sealed class BoobsResult {
    data class Success(val cachedFile: File) : BoobsResult()
    data class Failure(val error: String) : BoobsResult()
  }
}
