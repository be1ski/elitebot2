package data

import common.transliterate
import data.PropertiesRepo.Properties
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import java.nio.file.Files
import java.nio.file.StandardCopyOption.REPLACE_EXISTING
import javax.imageio.ImageIO

class CachedImageLoader(private val properties: Properties) {
  private val logger = LoggerFactory.getLogger(javaClass)

  fun load(url: String, fileName: String): LoadResult {
    val loadPath = Files.createTempFile(fileName, null)
    logger.info("Start loading $url to $loadPath")
    val connection = URL(url).openConnection().apply { setRequestProperty("User-Agent",
          "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2") }
    try { connection.getInputStream().use { Files.copy(it, loadPath, REPLACE_EXISTING) } }
    catch (e: Exception) { logger.info("Stream open failure $e") }
    val loadedFile = loadPath.toFile()
    if (loadedFile.length() > 0) { logger.info("Loaded file length > 0")
      return renderLoadedFile(loadedFile, fileName)
    } else { logger.info("Loaded file length == 0. Exiting.")
      return LoadResult.LoadFailure("Load failure")
    }
  }

  private fun renderLoadedFile(loadedFile: File, fileName: String): LoadResult {
    val targetDir = File("${properties.cacheDir}/${fileName.transliterate()}")
    logger.info("Start rendering file: $loadedFile to dir: $targetDir")
    targetDir.mkdirs()
    try {
      val renderedFile = ImageIO.read(loadedFile)
      logger.info("Rendering success")
      val jpgFile = File("$targetDir/${targetDir.list().size}.jpg")
      logger.info("Start writing rendered file to $jpgFile")
      ImageIO.write(renderedFile, "jpg", jpgFile)
      logger.info("Writing success")
      loadedFile.delete()
      logger.info("Tmp file $loadedFile removed")
      return LoadResult.LoadSuccess(jpgFile)
    } catch (e: Exception) {
      logger.info("An error occurs during rendering $e")
      return LoadResult.LoadFailure("Rendering failure")
    }
  }

  sealed class LoadResult {
    data class LoadSuccess(val file: File) : LoadResult()
    data class LoadFailure(val error: String) : LoadResult()
  }
}