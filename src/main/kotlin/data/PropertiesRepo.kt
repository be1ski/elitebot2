package data

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class PropertiesRepo {
  private val logger: Logger = LoggerFactory.getLogger(javaClass)
  private val propertiesFile = File("config.properties")
  val properties: Properties = loadProperties()

  init {
    logger.info("PropertiesRepo initialized with $properties")
  }

  private fun loadProperties(): Properties {
    if (!propertiesFile.exists()) {
      val defaultProperties = createDefaultProperties()
      defaultProperties.store(FileOutputStream(propertiesFile), "Elitebot properties")
      return defaultProperties.toProperties()
    }

    val properties = java.util.Properties()
    properties.load(FileInputStream(propertiesFile))
    return properties.toProperties()
  }

  private fun createDefaultProperties() = java.util.Properties().apply {
    put("BOT_USERNAME", "debug_elitobot")
    put("BOT_TOKEN", "187830246:AAEIBL-_yaSEYAB3fxu0lAqvwJj4gCaHjRI")
    put("CACHE_DIR", "data/cache")
    put("LOG_DIR", "data/logs")
  }

  private fun java.util.Properties.toProperties() =
      Properties(
          getProperty("BOT_USERNAME"),
          getProperty("BOT_TOKEN"),
          getProperty("CACHE_DIR"),
          getProperty("LOG_DIR")
      )

  data class Properties(
      val username: String,
      val token: String,
      val cacheDir: String,
      val logsDir: String
  )
}