package data.image

import data.image.ImageSeeker.SeekResult
import org.slf4j.LoggerFactory

class RandomImageSeeker : ImageSeeker {
  private val logger = LoggerFactory.getLogger(javaClass)

  private val seekers = listOf(
      YandexImageSeeker() to 15,
      MailRuImageSeeker() to 20,
      GoogleImageSeeker() to 20
  )

  init {
    logger.info("Initialized ImageSeekers: $seekers")
  }

  override fun seek(query: String): SeekResult {
    logger.info("Seek: $query")
    val nextSeeker = randomSeeker()
    logger.info("Selected seeker: ${nextSeeker.name()}")
    val result = nextSeeker.seek(query)
    logger.info("Result obtained: $result")
    return result
  }

  private fun randomSeeker(): ImageSeeker {
    val weightSum = seekers.sumByDouble { it.second.toDouble() }
    val randomWeight = Math.random() * weightSum
    var countWeight = 0.0
    for ((seeker, weight) in seekers) {
      countWeight += weight
      if (countWeight >= randomWeight)
        return seeker
    }
    throw NoSuchElementException()
  }
}