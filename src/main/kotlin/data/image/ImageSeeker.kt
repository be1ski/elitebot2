package data.image

interface ImageSeeker {
  fun seek(query: String): SeekResult
  fun name(): String = "Unknown"

  sealed class SeekResult {
    data class SeekSuccess(
        val query: String,
        val imageUrl: String) : SeekResult()

    data class SeekFailure(
        val message: String = "Попробуй снова") : SeekResult()
  }
}