package data.image

import data.image.ImageSeeker.SeekResult
import data.image.ImageSeeker.SeekResult.SeekFailure
import data.image.ImageSeeker.SeekResult.SeekSuccess
import org.jsoup.Jsoup
import java.net.URLEncoder
import java.util.Random

class YandexImageSeeker : ImageSeeker {
  private val BASE_URL = "https://yandex.ru/images/search?"
  private val USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0"

  private val random = Random()

  override fun name() = "Yandex"

  private var lastCookies: MutableMap<String, String>? = null


  override fun seek(query: String): SeekResult {
    try {
      val reqUrl = BASE_URL + "text=" + URLEncoder.encode(query)

      val connection = Jsoup.connect(reqUrl)
          .userAgent(USER_AGENT)

      if(lastCookies != null) {
        connection.cookies(lastCookies)
      }

      val response = connection.execute()

      lastCookies = response.cookies()


      val doc = response.parse()

      val imagesArray = doc.getElementsByClass("serp-item__thumb")

      val randomImage = imagesArray[random.nextInt(imagesArray.size)]
      val imageSrc = randomImage.absUrl("src")

      if(imageSrc.isEmpty()) {
        throw IllegalStateException()
      }

      if(imageSrc.contains("captcha")) {
        return SeekFailure("Яндекс просит капчу.")
      }

      return SeekSuccess(query, imageSrc)

    } catch (e: Exception) {
      return SeekFailure()
    }
  }
}
