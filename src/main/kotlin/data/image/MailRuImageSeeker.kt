package data.image

import data.image.ImageSeeker.SeekResult
import data.image.ImageSeeker.SeekResult.SeekFailure
import data.image.ImageSeeker.SeekResult.SeekSuccess
import org.json.JSONObject
import org.jsoup.Jsoup
import org.slf4j.LoggerFactory
import java.net.URLEncoder
import java.util.Random

class MailRuImageSeeker : ImageSeeker {
  private val random = Random()
  private val logger = LoggerFactory.getLogger(javaClass)

  override fun seek(query: String): SeekResult {
   try {
     val query = URLEncoder.encode(query)

     logger.info("Get https://go.mail.ru/search_images?q=$query&fm=1")

     val doc = Jsoup.connect("https://go.mail.ru/search_images?q=$query&fm=1").get()

     val rootJson = doc.select("script").first { it.toString().contains("STP.results") }.toString()
         .split("STP.results =")[1].split("</script>")[0].removeLast()
     val items = JSONObject(rootJson).getJSONArray("items")
     return SeekSuccess(query, items.getJSONObject(random.nextInt(items.length())).getString("imUrl"))
   } catch (e: Exception) {
     return SeekFailure()
   }
  }

  override fun name() = "Mail.ru"

  private fun String.removeLast(): String = this.substring(0, length - 1)
}