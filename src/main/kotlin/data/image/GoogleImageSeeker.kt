package data.image

import data.image.ImageSeeker.SeekResult
import data.image.ImageSeeker.SeekResult.SeekFailure
import data.image.ImageSeeker.SeekResult.SeekSuccess
import okhttp3.OkHttpClient
import okhttp3.Request.Builder
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.net.URLEncoder
import java.util.Random

class GoogleImageSeeker : ImageSeeker {
  private val logger = LoggerFactory.getLogger(javaClass)

  private val okHttpClient = OkHttpClient()

  private val BASE_URL = "https://www.googleapis.com/customsearch/v1?"
  private val API_KEY = "AIzaSyAUn4MNnLEt9AMxAPSaMNuTQkDcyROUnpI"
  private val CX = "006971661295744138938:0hcmjkbpkqy"

  override fun seek(query: String): SeekResult {

    try {
      val reqUrl = BASE_URL + "key=" + API_KEY + "&cx=" + CX + "&q=" + URLEncoder.encode(query) + "&searchType=image"

      logger.info("Get $reqUrl")

      val response = okHttpClient.newCall(Builder().url(reqUrl).build()).execute()

      val jsonObject = JSONObject(response.body()!!.string())

      val items = jsonObject.getJSONArray("items")
      val SELECT_ITEMS = Math.min(items.length(), 10)
      val randomIndex = Random().nextInt(Math.min(items.length(), SELECT_ITEMS))
      val imageSrc = items.getJSONObject(randomIndex).getString("link")

      return SeekSuccess(query, imageSrc)
    } catch (e: Exception) {
      return SeekFailure()
    }

  }

  override fun name() = "Google"
}
