package data

import data.CachedImageLoader.LoadResult
import data.CachedImageLoader.LoadResult.LoadFailure
import data.CachedImageLoader.LoadResult.LoadSuccess
import java.io.File
import java.util.Random
import org.json.JSONArray
import org.apache.http.impl.client.BasicResponseHandler
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

class ButtsRepo(properties: PropertiesRepo.Properties) {
  private val logger = LoggerFactory.getLogger(javaClass)
  private val httpClient = HttpClientBuilder.create().build()
  private val imageLoader = CachedImageLoader(properties)
  private val random = Random()

  private var lastSync = 0L
  private val maxButtsId = fetchMaxButtsId()

  init {
    logger.info("ButtsRepo initialized")
  }

  fun nextButts() : ButtsResult {
    if((System.currentTimeMillis() - lastSync) > TimeUnit.DAYS.toMillis(1L)) {
      fetchMaxButtsId()
    }
    logger.info("Throw a coin...")
    val nextButtsId = random.nextInt(maxButtsId) + 7
    logger.info("Next buttsId: $nextButtsId")
    val loadResult =
        imageLoader.load(String.format("http://media.obutts.ru/butts/%05d.jpg", nextButtsId), "butts")
    return processLoadResult(loadResult)
  }

  private fun processLoadResult(loadResult: LoadResult) = when(loadResult) {
    is LoadSuccess -> ButtsResult.Success(loadResult.file)
    is LoadFailure -> ButtsResult.Failure("Попробуй снова")
  }

  private fun fetchMaxButtsId(): Int {
    logger.info("Start fetching maxButtsId")
    try { val responseBody = httpClient.execute(HttpGet("http://api.obutts.ru/butts/0/1/-id"), BasicResponseHandler())
      val result = JSONArray(responseBody).getJSONObject(0).getInt("id")
      logger.info("Fetch maxButtsId success: $result")
      lastSync = System.currentTimeMillis()
      return result
    } catch (e: Exception) { logger.info("maxButtsId fetching failed $e") }
    return 0
  }

  sealed class ButtsResult {
    data class Success(val cachedFile: File) : ButtsResult()
    data class Failure(val error: String) : ButtsResult()
  }
}
