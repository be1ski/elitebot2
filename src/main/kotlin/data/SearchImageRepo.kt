package data

import data.CachedImageLoader.LoadResult
import data.CachedImageLoader.LoadResult.LoadFailure
import data.CachedImageLoader.LoadResult.LoadSuccess
import data.SearchImageRepo.SearchResult.Failure
import data.image.ImageSeeker.SeekResult
import data.image.ImageSeeker.SeekResult.SeekFailure
import data.image.ImageSeeker.SeekResult.SeekSuccess
import data.image.RandomImageSeeker
import java.io.File

class SearchImageRepo(properties: PropertiesRepo.Properties) {
  private val imageLoader = CachedImageLoader(properties)
  private val imageSeeker = RandomImageSeeker()

  fun search(query: String) = processSeekResult(imageSeeker.seek(query), query)

  private fun processSeekResult(seekResult: SeekResult, query: String): SearchResult = when (seekResult) {
    is SeekSuccess -> processSeekSuccess(seekResult, query)
    is SeekFailure -> processSeekFailure(seekResult)
  }

  private fun processSeekSuccess(seekSuccess: SeekSuccess, query: String) =
      processLoadResult(imageLoader.load(seekSuccess.imageUrl, query))

  private fun processSeekFailure(seekFailure: SeekFailure) =
      Failure(seekFailure.message)

  private fun processLoadResult(loadResult: LoadResult) = when (loadResult) {
    is LoadSuccess -> SearchResult.Success(loadResult.file)
    is LoadFailure -> SearchResult.Failure("Попробуй снова")
  }

  sealed class SearchResult {
    data class Success(val cachedFile: File) : SearchResult()
    data class Failure(val error: String) : SearchResult()
  }
}