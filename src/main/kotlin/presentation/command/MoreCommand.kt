package presentation.command

import org.telegram.telegrambots.api.objects.Message
import presentation.CommandExecutorHolder

class MoreCommand(private val executorHolder: CommandExecutorHolder,
    private val aliases: List<String>) : Command {

  override fun match(message: Message) =
      aliases.any { message.text.equals(it, ignoreCase = true) }

  override fun createExecutor(message: Message) = executorHolder.lastExecutor

  companion object Factory {
    fun create(executorHolder: CommandExecutorHolder) =
        MoreCommand(executorHolder, listOf("еще", "ещё", "ищо"))
  }
}