package presentation.command

import data.BoobsRepo
import data.BoobsRepo.BoobsResult
import data.BoobsRepo.BoobsResult.Failure
import data.BoobsRepo.BoobsResult.Success
import data.PropertiesRepo
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.send.SendPhoto
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import presentation.command.Command.CommandExecutor

class BoobsCommand(properties: PropertiesRepo.Properties,
    private val bot: TelegramLongPollingBot) : Command {

  private val boobsRepo = BoobsRepo(properties)

  override fun match(message: Message) = message.text.equals("сиси", ignoreCase = true)

  override fun createExecutor(message: Message) = BoobsSendExecutor(message.chatId, message.messageId)

  inner class BoobsSendExecutor(private val chatId: Long, private val replyTo: Int) : CommandExecutor {
    override fun execute() = processBoobsResult(boobsRepo.nextBoobs())

    private fun processBoobsResult(boobsResult: BoobsResult) = when (boobsResult) {
      is BoobsResult.Success -> sendPhoto(boobsResult, chatId, replyTo)
      is BoobsResult.Failure -> sendError(boobsResult)
    }

    private fun sendPhoto(success: Success, chatId: Long, replyTo: Int) {
      bot.sendPhoto(SendPhoto().apply {
        setReplyToMessageId(replyTo)
        setChatId(chatId)
        setNewPhoto(success.cachedFile)
      })
    }

    private fun sendError(failure: Failure) {
      bot.sendMessage(SendMessage(chatId, failure.error))
    }
  }

  companion object Factory {
    fun create(properties: PropertiesRepo.Properties, bot: TelegramLongPollingBot)
        = BoobsCommand(properties, bot)
  }
}