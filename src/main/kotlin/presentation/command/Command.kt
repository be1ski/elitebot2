package presentation.command

import org.telegram.telegrambots.api.objects.Message

interface Command {
  fun match(message: Message): Boolean
  fun createExecutor(message: Message): CommandExecutor

  interface CommandExecutor {
    fun execute()
  }
}