package presentation.command

import data.PropertiesRepo.Properties
import data.SearchImageRepo
import data.SearchImageRepo.SearchResult
import data.SearchImageRepo.SearchResult.Failure
import data.SearchImageRepo.SearchResult.Success
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.send.SendPhoto
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import presentation.command.Command.CommandExecutor

class PictureCommand(properties: Properties,
    private val bot: TelegramLongPollingBot, private val aliases: List<String>) : Command {

  private val imageRepo = SearchImageRepo(properties)

  override fun match(message: Message) =
      aliases.any { message.text.startsWith(it, ignoreCase = true) }

  override fun createExecutor(message: Message): CommandExecutor {
    val query = message.text.toLowerCase().split(
        delimiters = aliases.first { message.text.toLowerCase().startsWith(it) })[1]

    return PictureSendExecutor(query, message.chatId, message.messageId)
  }

  inner class PictureSendExecutor(private val query: String, private val chatId: Long,
      private val replyTo: Int) : CommandExecutor {

    override fun execute() = processSearchResult(imageRepo.search(query))

    private fun processSearchResult(search: SearchResult) = when (search) {
      is Success -> sendPhoto(search, replyTo, chatId)
      is Failure -> sendError(search)
    }

    private fun sendPhoto(success: Success, replyTo: Int, chatId: Long) {
      bot.sendPhoto(SendPhoto().apply {
        setChatId(chatId)
        setReplyToMessageId(replyTo)
        setNewPhoto(success.cachedFile)
      })
    }

    private fun sendError(failure: Failure) {
      bot.sendMessage(SendMessage(chatId, failure.error))
    }
  }

  companion object Factory {
    fun create(properties: Properties, bot: TelegramLongPollingBot) =
        PictureCommand(properties, bot, listOf("пикча ", "п ", "/img "))
  }
}