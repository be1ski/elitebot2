package presentation.command

import org.telegram.telegrambots.api.objects.Message
import java.util.Random

class RollCommand(private val pictureCommand: PictureCommand,
    private val randomXXXCommand: RandomXXXCommand, private val aliases: List<String>) : Command {

  private val random = Random()
  private val wtfQueries = listOf(
      "жесть",
      "ебать ты лох",
      "disgusting",
      "gross",
      "guro",
      "гуро",
      "расчлененка"
  )

  override fun match(message: Message) =
      aliases.any { message.text.equals(it, ignoreCase = true) }

  override fun createExecutor(message: Message) = when (random.nextInt(6)) {
    in 1..5 -> randomXXXCommand.createExecutor(message)
    else    -> pictureCommand.PictureSendExecutor(randomWtfQuery(), message.chatId, message.messageId)
  }

  private fun randomWtfQuery() = wtfQueries[random.nextInt(wtfQueries.size)]

  companion object Factory {
    fun create(pictureCommand: PictureCommand, randomXXXCommand: RandomXXXCommand) =
        RollCommand(pictureCommand, randomXXXCommand, listOf("/roll", "/кручу"))
  }
}
