package presentation.command

import org.telegram.telegrambots.api.objects.Message
import java.util.Random

class RandomXXXCommand(private val buttsCommand: ButtsCommand, private val boobsCommand: BoobsCommand,
    private val aliases: List<String>) : Command {

  private val random = Random()

  override fun match(message: Message) =
      aliases.any { message.text.equals(it, ignoreCase = true) }

  override fun createExecutor(message: Message) =
      if (random.nextBoolean()) buttsCommand.createExecutor(message) else boobsCommand.createExecutor(message)

  companion object Factory {
    fun create(buttsCommand: ButtsCommand, boobsCommand: BoobsCommand) =
        RandomXXXCommand(buttsCommand, boobsCommand, listOf("х", "x", "взгрустнул!"))
  }
}