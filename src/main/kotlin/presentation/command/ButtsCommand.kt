package presentation.command

import data.ButtsRepo
import data.ButtsRepo.ButtsResult
import data.ButtsRepo.ButtsResult.Failure
import data.ButtsRepo.ButtsResult.Success
import data.PropertiesRepo
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.send.SendPhoto
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import presentation.command.Command.CommandExecutor

class ButtsCommand(properties: PropertiesRepo.Properties,
    private val bot: TelegramLongPollingBot) : Command {

  private val buttsRepo = ButtsRepo(properties)

  override fun match(message: Message) = message.text.equals("попа", ignoreCase = true)

  override fun createExecutor(message: Message) = ButtsSendExecutor(message.chatId, message.messageId)

  inner class ButtsSendExecutor(private val chatId: Long, private val replyTo: Int) : CommandExecutor {
    override fun execute() = processButtsResult(buttsRepo.nextButts())

    private fun processButtsResult(buttsResult: ButtsResult) = when (buttsResult) {
      is ButtsResult.Success -> sendPhoto(buttsResult, chatId, replyTo)
      is ButtsResult.Failure -> sendError(buttsResult)
    }

    private fun sendPhoto(success: Success, chatId: Long, replyTo: Int) {
      bot.sendPhoto(SendPhoto().apply {
        setReplyToMessageId(replyTo)
        setChatId(chatId)
        setNewPhoto(success.cachedFile)
      })
    }

    private fun sendError(failure: Failure) {
      bot.sendMessage(SendMessage(chatId, failure.error))
    }
  }

  companion object Factory {
    fun create(properties: PropertiesRepo.Properties, bot: TelegramLongPollingBot)
        = ButtsCommand(properties, bot)
  }
}