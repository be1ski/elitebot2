package presentation

import presentation.command.Command.CommandExecutor
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors

class CommandExecutorHolder {
  private val logger = LoggerFactory.getLogger(javaClass)
  private val threadPool = Executors.newFixedThreadPool(25)

  var lastExecutor: CommandExecutor = DefaultExecutor

  init {
    logger.info("Initialized thread pool: $threadPool")
  }

  fun putAndExecute(executor: CommandExecutor) {
    logger.info("Put to queue: $executor")
    lastExecutor = executor

    executeInExecutor(executor)
  }

  private fun executeInExecutor(commandExecutor: CommandExecutor) = threadPool.execute {
    logger.info("Execute in executor: $threadPool")
    logger.info("Thread: ${Thread.currentThread()}")
    commandExecutor.execute()
  }

  private companion object DefaultExecutor: CommandExecutor {
    override fun execute() {}
  }
}