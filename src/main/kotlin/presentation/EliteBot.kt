package presentation

import data.PropertiesRepo.Properties
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import presentation.command.BoobsCommand
import presentation.command.ButtsCommand
import presentation.command.MoreCommand
import presentation.command.PictureCommand
import presentation.command.RandomXXXCommand
import presentation.command.RollCommand

class EliteBot(private val properties: Properties) : TelegramLongPollingBot() {
  private val logger = LoggerFactory.getLogger(javaClass)
  private val executorHolder = CommandExecutorHolder()

  private val moreCommand = MoreCommand.create(executorHolder)
  private val boobsCommand = BoobsCommand.create(properties, this)
  private val buttsCommand = ButtsCommand.create(properties, this)
  private val pictureCommand = PictureCommand.create(properties, this)
  private val randomXXXCommand = RandomXXXCommand.create(buttsCommand, boobsCommand)
  private val rollCommand = RollCommand.create(pictureCommand, randomXXXCommand)

  private val commands = listOf(
      pictureCommand, boobsCommand, buttsCommand, rollCommand, randomXXXCommand, moreCommand)

  init {
    logger.info("EliteBot initialized with executorHolder: $executorHolder")
    logger.info("Commands: $commands")
  }

  override fun getBotUsername() = properties.username
  override fun getBotToken() = properties.token

  override fun onUpdateReceived(update: Update) {
    logger.info("Update received: ${update.message}")

    commands.filter { it.match(update.message) }
        .forEach { executorHolder.putAndExecute(it.createExecutor(update.message)) }
  }
}